import React, { useState } from 'react';
import { Router } from '@reach/router';
import { render } from 'react-dom';
import ThemeContext from './ThemeContext';
import NavBar from './NavBar';
import Details from './Details';
import SearchParams from './SearchParams';
const App = () => {
  const themeHook = useState('peru  `1');
  return (
    <React.StrictMode>
      <ThemeContext.Provider value={themeHook}>
        <div>
          <NavBar />
          <Router>
            <SearchParams path="/" />
            <Details path="/details/:id" />
          </Router>
        </div>
      </ThemeContext.Provider>
    </React.StrictMode>
  );
};

render(<App />, document.getElementById('root'));
