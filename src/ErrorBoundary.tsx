import React, { Component, ErrorInfo } from 'react';
import { Link, Redirect } from '@reach/router';

export class ErrorBoundary extends Component {
  state = { hasError: false, redirect: false };
  static getDerivedStateFromError() {
    return { hasError: true };
  }
  componentDidCatch(error: Error, info: ErrorInfo) {
    console.log('Error Boundary caught an error: ', error, info);
  }
  componentDidUpdate() {
    if (this.state.hasError) {
      setTimeout(() => this.setState({ redirect: true }), 5000);
    }
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
    if (this.state.hasError) {
      return (
        <h1>
          There is an error with this listing{' '}
          <span role="img" aria-label="oops emoji">
            🤦‍🤷‍♂️
          </span>{' '}
          <Link to="/">Click here </Link>or wait for five seconds to be
          redirected to Home Page
          <span role="img" aria-label="high-five emoji">
            🙌
          </span>
        </h1>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
